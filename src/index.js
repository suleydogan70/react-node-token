import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import WebFont from 'webfontloader'
import store from './store'
import './style.scss'

import Routes from './routes'

WebFont.load({
  google: {
    families: ['Open+Sans:300,300i,400,600,700,800']
  }
})

const App = () => (
  <Provider store={store}>
    <Routes />
  </Provider>
)

ReactDOM.render(<App />, document.getElementById('app'))
