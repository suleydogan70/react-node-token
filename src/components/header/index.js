import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => (
  <header>
    <div className="wrap">
      <div className="header_left">
        <div id="logo">
          <Link to="/" title="Treelogism"><img src="src/img/logo.png" alt="Treelogism" /></Link>
        </div>
        <div id="search">
          <input type="text" placeholder="Rechercher..." />
        </div>
      </div>
      <div className="header_right">
        <nav>
          <ul>
            <li>
              <Link className="picto_publication" to="/">
                <span className="intitule">nouveau</span>
              </Link>
            </li>
            <li>
              <Link className="picto_amis" to="/">
                <span className="intitule">amis</span>
              </Link>
            </li>
            <li>
              <Link className="picto_map" to="/">
                <span className="intitule">carte</span>
              </Link>
            </li>
            <li>
              <Link className="picto_notification" to="/">
                <span className="intitule">notifs</span>
              </Link>
            </li>
          </ul>
        </nav>
        <div id="profil">
          <Link to=""><img src="src/img/profil.jpg" alt="profil" /></Link>
        </div>
      </div>
    </div>
  </header>
)

export default Header
