import { fromJS } from 'immutable'

import actionsType from '../actions/actions-type'

const DEFAULT_STATE = {
  isAuth: false,
  token: null,
  errorMessage: null,
  user: null
}

const getConnectedUser = (state, action) => (
  fromJS(state)
    .setIn(['isAuth'], true)
    .setIn(['token'], action.data.token)
    .setIn(['user'], action.data.user)
    .toJS()
)

const connectError = (state, action) => (
  fromJS(state)
    .setIn(['isAuth'], false)
    .setIn(['token'], null)
    .setIn(['user'], null)
    .setIn(['errorMessage'], action.data)
    .toJS()
)

const connexion = (state = DEFAULT_STATE, action) => {
  switch (action.type) {
    case actionsType.CONNECT_USER:
      return getConnectedUser(state, action)
    case actionsType.CONNECT_ERROR:
      return connectError(state, action)
    default:
      return state
  }
}

export default connexion
