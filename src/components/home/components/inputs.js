import React, { Component } from 'react'

class Input extends Component {
  render() {
    const {
      input: { value, onChange },
      id,
      label,
      name,
      placeholder,
      type
    } = this.props
    return (
      <div>
        <label htmlFor={id}>{label}</label>
        <input
          name={name}
          id={id}
          placeholder={placeholder}
          type={type}
          value={value}
          onChange={onChange}
        />
      </div>
    )
  }
}

export default Input
