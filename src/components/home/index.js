import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { reduxForm, Field } from 'redux-form'
import { connect } from 'react-redux'
import { compose } from 'redux'
import * as actions from './actions'
import Input from './components/inputs'

class Home extends Component {
  constructor(props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
    this.state = {
      mail: '',
      pass: '',
      submit: true
    }
  }

  onSubmit(formData) {
    const { connectUser } = this.props
    connectUser(formData)
  }

  handleChange(e) {
    if (e.target.id === 'mail') {
      this.state.mail = e.target.value
    }
    if (e.target.id === 'pass') {
      this.state.pass = e.target.value
    }
    const { mail, pass } = this.state
    if (mail.length > 5 && pass.length > 5) {
      this.setState({ submit: false })
    } else {
      this.setState({ submit: true })
    }
  }

  render() {
    const { handleSubmit, auth: { isAuth, errorMessage } } = this.props
    const { submit } = this.state
    if (isAuth) {
      return <Redirect to="/lol" />
    }
    return (
      <div>
        <form onSubmit={handleSubmit(this.onSubmit)}>
          <Field
            name="mail"
            type="email"
            id="mail"
            component={Input}
            placeholder="Adresse mail"
            label="Identifiant"
            onChange={e => this.handleChange(e)}
          />
          <Field
            name="pass"
            type="password"
            id="pass"
            component={Input}
            placeholder="Mot de passe"
            label="Mot de passe"
            onChange={e => this.handleChange(e)}
          />
          <p>{errorMessage}</p>
          <button type="submit" disabled={submit}>Connexion</button>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth
  }
}

export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'connexion' })
)(Home)
