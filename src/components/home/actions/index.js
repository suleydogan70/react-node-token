import axios from 'axios'
import actionsType from './actions-type'

export const connectUser = formData => (
  (dispatch) => {
    axios.post('http://localhost:1234/user/connexion', formData)
      .then(
        (response) => {
          console.log(response)

          if (response.data.err) {
            return dispatch({
              type: actionsType.CONNECT_ERROR,
              data: response.data.err
            })
          }
          dispatch({
            type: actionsType.CONNECT_USER,
            data: response.data
          })
          return localStorage.setItem('token', response.data.token)
        },
        () => {
          dispatch({
            type: actionsType.CONNECT_ERROR,
            data: 'An error has occured'
          })
        }
      )
  }
)
