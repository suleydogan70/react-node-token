const express = require('express')
const JWT = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const userRoute = express.Router()
const connexion = require('../connexion')
const userCtrl = require('../controllers/user')
const { JWT_SECRET } = require('../configs')
const passwordRegex = new RegExp(`^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})`)

signToken = userId => {
  return JWT.sign({
    iss: 'Splitziz',
    sub: userId
  }, JWT_SECRET)
}

userRoute.post('/connexion', (req, res) => {
  const { mail, pass } = req.body
  // Terminate the execution if one field is empty
  if (mail === undefined || mail === null)
    return res.json({ err: `The email field is empty` })
  if (pass === undefined || pass === null)
    return res.json({ err: `The password field is empty` })
  // check if the mail is used, to stop the function if it's not
  userCtrl.checkUserExist(mail)
  .then(
    (response) => {
      // if we can't get the User's datas, then the account doesn't exist
      if(response[0] === undefined)
        return res.json({ err: `This account doesn't exist` })
      bcrypt.compare(pass, response[0].user_psw, function(err, result) {
        if(!result) {
          return res.status(200).json({ err: `You typed a wrong password` })
        }
        userCtrl.findUserById(response[0].user_id)
        .then(
          (data) => {
            const token = signToken(data[0].user_id)
            res.status(200).json({ token, user: data[0] })
          },
          (error) => {
            res.status(200).json({ err: `An error has occured while trying to get the User's datas` })
          }
        )
      })
    },
    (error) => {
      res.status(200).json({ err: `An error has occured while trying to check if the User exist` })
    }
  )
})

userRoute.post('/inscription', (req, res) => {
  const { mail, pass, firstname, lastname } = req.body
  // Terminate the execution if one field is empty
  if (mail === undefined || mail === null)
    return res.json({ err: `The email field is empty` })
  if (pass === undefined || pass === null)
    return res.json({ err: `The password field is empty` })
  if (firstname === undefined || firstname === null)
    return res.json({ err: `The firstname field is empty` })
  if (lastname === undefined || lastname === null)
    return res.json({ err: `The lastname field is empty` })
  // If the user mail already exist in the db
  userCtrl.checkUserExist(mail).then(
    async (response) => {
      if(response[0] !== undefined)
        return res.json({ err: `This email is already used` })
      // If the password field doesn't respect the RegExp, return error
      if(!passwordRegex.test(pass))
        return res.json({
          err: `The password must contain more than 6 characters, 1 special character, 1 lowercase character, 1 uppercase character and 1 numeric character`
        })
      const salt = await bcrypt.genSalt(10)
      const hashedPassword = await bcrypt.hash(pass, salt)
      userCtrl.addUser(mail, hashedPassword, firstname, lastname)
      .then(
        (response) => {
          // If the User is not created, send error
          if(response.insertId === undefined || response.insertId === null)
            return res.json({ err: `An error has occured` })
          // generate token
          const token = signToken(response.insertId)
          // get the added user's data and send them with connexion token
          userCtrl.findUserById(response.insertId)
          .then(
            (response) => {
              res.status(200).json({ token, user: response[0] })
            },
            (error) => {
              res.status(200).json({ err: `An error has occured while trying to find the User` })
            }
          )
        },
        (error) => {
          res.status(200).json({ err: `An error has occured while trying to save the User` })
        }
      )
    },
    (error) => {
      res.status(200).json({ err: `An error has occured while trying to check if the User exist` })
    }
  )
})

module.exports = userRoute
