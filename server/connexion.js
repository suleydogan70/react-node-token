const mysql = require('mysql')
const util = require('util')
const connexion = mysql.createPool({
  connectionLimit: 10,
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'Treelogism'
})
connexion.query = util.promisify(connexion.query)

module.exports = connexion
