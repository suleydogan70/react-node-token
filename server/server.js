const express = require('express')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const app = express()
const userRoutes = require('./routes/user')

/* --- MiddleWares --- */
app.use(bodyParser.urlencoded({extended: true})) // access post datas by req.body.THENAME
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
      imgSrc: ['*'],
      upgradeInsecureRequests: true,
    },
  })
)
app.use(helmet.frameguard({ action: 'deny' }))
app.use(helmet.noSniff())
app.use(
  helmet.hsts({ maxAge: 31536000, includeSubDomains: true, preload: true })
)
app.use(helmet.ieNoOpen())
app.use(helmet.referrerPolicy({ policy: 'no-referrer' }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*")
    res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE')
    res.header('Access-Control-Allow-Headers', 'Content-Type')
    next()
})
app.use('/user', userRoutes)
/* --- MiddleWares --- */

app.listen(1234)
