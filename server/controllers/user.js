const connexion = require('../connexion')

const userCtrl = {
  async checkUserExist(mail) {
    const queryString = 'select u.user_id, u.user_psw from users u where u.user_mail = ?'
    return await connexion.query(queryString, [mail])
  },
  async addUser(mail, pass, firstname, lastname) {
    console.log('la fonction addUser est appelé', mail, pass, firstname, lastname)
    const queryString = `insert into users(
      user_mail,
      user_psw,
      user_firstname,
      user_lastname
    )
    values (?, ?, ?, ?)`

    return await connexion.query(queryString, [mail, pass, firstname, lastname])
  },
  async findUserById(user_id) {
    const queryString = `select
    user_id,
    user_mail,
    user_firstname,
    user_lastname,
    user_description,
    admin, user_leafs,
    title_id
    from users u where u.user_id = ?`
    return await connexion.query(queryString, [user_id])
  }
}

module.exports = userCtrl
